const getUserChoice = userInput => {
  return userInput = userInput.toLowerCase();
  if (userInput === 'rock' || userInput === 'paper' || userInput === 'scissors') {
    return userInput;
  } else {
    console.log('Invalid input');
  }
}

  const getComputerChoice = () => {
    let randomNumber = Math.floor(Math.random() * 3);
    switch (randomNumber) {
      case 0:
        return 'rock';
      case 1:
        return 'paper';
      case 2:
        return 'scissors';
                        }
  }
const determineWinner = (userChoice, computerChoice) => {
  if (userChoice === computerChoice) {
    return 'Tie';
  } if (userChoice === 'rock') {
    if (computerChoice === 'scissors') {
      return 'User won';
    } else {
      return 'Computer won';
    } if (userChoice === 'paper') {
      if (computerChoice === 'rock') {
        return 'User won'; 
        } else {
          return 'Computer won';
        } if (userChoice === 'scissors') {
          if (computerChoice === 'paper') {
            return 'User won';
          } else {
            return 'Computer won';
          }
        }
      }
    }
  }
const playGame = () => {
  const userChoice = getUserChoice('scissors');
  const computerChoice = getComputerChoice();
  console.log('You chose: ' + userChoice);
  console.log('The computer chose: ' + computerChoice);
  console.log(determineWinner(userChoice, computerChoice));
}

playGame();